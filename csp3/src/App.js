import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import AppNavBar from "./components/AppNavBar";
import Home from "./pages/Home";
import Home_Placeholder from "./pages/Home_Placeholder";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Products from "./pages/Products";
import Specific from "./pages/Specific";
import MyCart from "./pages/MyCart";
import Orders from "./pages/Orders";
import Error from "./pages/Error";
import Reports from "./pages/Reports";
import "./App.css";
import "bootswatch/dist/cosmo/bootstrap.min.css";
import { UserProvider } from "./UserContext";
import Footer from "./components/Footer";
import MessengerCustomerChat from "react-messenger-customer-chat";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const [searchProduct, setSearchProduct] = useState([]);

  useEffect(() => {
    fetch("https://finalcsp.herokuapp.com/users/details", {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data._id !== "undefined") {
          setUser({ id: data._id, isAdmin: data.isAdmin });
        } else {
          setUser({ id: null, isAdmin: null });
        }
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, searchProduct, setSearchProduct }}>
      <Router>
        <AppNavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          {/* <Route path="/" element={<Home_Placeholder />} /> */}
          <Route path="/reports" element={<Reports />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/products" element={<Products />} />
          <Route path="/products/:productId" element={<Specific />} />
          <Route path="/cart" element={<MyCart />} />
          <Route path="/orders" element={<Orders />} />
          <Route path="/logout" element={<Logout />} />
          {/* <Route element={Error} /> */}
        </Routes>
        <MessengerCustomerChat
          pageId="105006395474643"
          appId="1024093251477673"
        />

        <Footer />
      </Router>
    </UserProvider>
  );
}

export default App;

import React from "react";
import {
	Paper,
	Typography,
	Grid,
	Stack,
	Divider,
	Button,
	TextField,
} from "@mui/material";
import AccessTimeFilledRoundedIcon from "@mui/icons-material/AccessTimeFilledRounded";
import PhoneIphoneRoundedIcon from "@mui/icons-material/PhoneIphoneRounded";
import BusinessRoundedIcon from "@mui/icons-material/BusinessRounded";
import FacebookRoundedIcon from "@mui/icons-material/FacebookRounded";
import ShareTwoToneIcon from "@mui/icons-material/ShareTwoTone";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";

const Footer = () => {
	//PLease add spacing={4} refer to docs
	return (
		<div className="text-left mt-5 customFooter">
			<Grid container>
				<Grid item md={3}>
					<Stack spacing={3}>
						<Typography variant="h6">
							<div style={{ color: "#cf2128" }}>About Us</div>
						</Typography>
						<Typography variant="subtitle2">
							<div style={{ color: "#5c5c5c" }}>
								The go-to store for all things tech. We ship
								nationwide and accepts bulk olders with
								discounts.
							</div>
						</Typography>
					</Stack>
				</Grid>
				<Grid item md={3}>
					<Stack spacing={3}>
						<Typography variant="h6">
							<div style={{ color: "#cf2128" }}>
								Store location
							</div>
						</Typography>

						<Typography variant="subtitle2">
							<div style={{ color: "#5c5c5c" }}>
								<BusinessRoundedIcon />
								Caswynn Building, 3rd Floor, Quezon City
							</div>
						</Typography>

						<Typography variant="subtitle2">
							<div style={{ color: "#5c5c5c" }}>
								<PhoneIphoneRoundedIcon />
								0917 166 8714/0932 868 8713
							</div>
						</Typography>

						<Typography variant="subtitle2">
							<div style={{ color: "#5c5c5c" }}>
								<AccessTimeFilledRoundedIcon />
								Mon-Sat: 9AM to 10PM
							</div>
						</Typography>
					</Stack>
				</Grid>

				<Grid item md={3}>
					<Stack spacing={3}>
						<Typography variant="h6">
							<div style={{ color: "#cf2128" }}>News</div>
						</Typography>
						<Typography variant="subtitle2">
							<div style={{ color: "#5c5c5c" }}>
								Enter your e-mail address to get the latest
								deals and last minute sale!
							</div>
						</Typography>
						<Stack
							direction="row"
							spacing={2}
							className="text-center"
						>
							<TextField
								id="outlined-basic"
								label="Email Address"
								variant="outlined"
							/>
							<Button variant="outlined">
								Submit {"   "} <EmailOutlinedIcon />
							</Button>
						</Stack>
					</Stack>
				</Grid>
				<Grid item md={3}>
					<Stack spacing={3}>
						<Typography variant="h6">
							<div style={{ color: "#cf2128" }}>Follow us</div>
						</Typography>
						<Stack
							direction="row"
							divider={
								<Divider orientation="vertical" flexItem />
							}
						>
							<FacebookRoundedIcon />
							<Divider />
							<ShareTwoToneIcon />
						</Stack>
					</Stack>
				</Grid>
			</Grid>
		</div>
	);
};
export default Footer;

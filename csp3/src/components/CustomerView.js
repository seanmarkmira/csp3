import React, { useEffect, useState } from "react";
import Product from "../components/Product";
import { Row } from "react-bootstrap";
import CarousellComp from "../components/CarousellComp";

// Where is the productsData coming from here? Pages: Products.js
/*
    1. When click pass the arguement here
    2. Put if statement and add 2 use effect.
    3. The if will be: if the coming data is type of String, fetch fuzzy search, else productsData.Map
    4. Result of fuzzy search will be the same
*/
const CustomerView = ({ productsData }) => {
    const [products, setProducts] = useState([]);
    useEffect(() => {
        const productsArr = productsData.map((productData) => {
            if (productData.isActive === true) {
                return (
                    <Product
                        data={productData}
                        key={productData._id}
                        breakPoint={4}
                    />
                );
            } else {
                return null;
            }
        });

        setProducts(productsArr);
    }, [productsData]);

    return (
        <React.Fragment>
            <h2 className="text-center my-4">Our Products</h2>
            <CarousellComp />
            <Row>{products}</Row>
        </React.Fragment>
    );
};
export default CustomerView;

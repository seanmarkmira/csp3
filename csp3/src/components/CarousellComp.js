import React from "react";
import { Carousel, Container, Row, Col } from "react-bootstrap";

const CarousellComp = () => {
	return (
		<>
			<Container>
				{/* <Row md={2} xs={12}></Row> */}
				<Row xs={12} md={12}>
					<Carousel fade className="carousel">
						<Carousel.Item>
							<img
								className="w-100 img_carousel"
								src="https://www.asus.com/events/eventes/epic/2825_1900.jpg"
								// alt="First slide"
							/>
							<Carousel.Caption>
								{/* <h3>First slide label</h3> */}
								{/* <p> */}
								{/* 	Nulla vitae elit libero, a pharetra */}
								{/* 	augue mollis interdum. */}
								{/* </p> */}
							</Carousel.Caption>
						</Carousel.Item>
						<Carousel.Item>
							<img
								className="w-100 img_carousel"
								src="https://www.asus.com/events/eventes/epic/2887_1900.jpg"
								// alt="Second slide"
							/>

							<Carousel.Caption>
								{/* <h3>Second slide label</h3> */}
								{/* <p> */}
								{/* 	Lorem ipsum dolor sit amet, consectetur */}
								{/* 	adipiscing elit. */}
								{/* </p> */}
							</Carousel.Caption>
						</Carousel.Item>
						<Carousel.Item>
							<img
								className="w-100 img_carousel"
								src="https://cdn.wccftech.com/wp-content/uploads/2020/05/2095_1900-1.jpg"
								// alt="Third slide"
							/>

							<Carousel.Caption>
								{/* <h3>Third slide label</h3> */}
								{/* <p> */}
								{/* 	Praesent commodo cursus magna, vel */}
								{/* 	scelerisque nisl consectetur. */}
								{/* </p> */}
							</Carousel.Caption>
						</Carousel.Item>
					</Carousel>
				</Row>
			</Container>
		</>
	);
};
export default CarousellComp;

import React, { useEffect, useState } from "react";
import { Form, Table, Button, Modal, Card } from "react-bootstrap";

import Accordion from "react-bootstrap/Accordion";
import { Link } from "react-router-dom";
import moment from "moment";
//Agenda:
// 1. we must be able to add data from admin dashboard
// 2. we must be able to update data from admin dashboard
// 3. we must be able to fetch data from admin dashbaord for new data: imageURL and rating
const AdminView = (props) => {
	const { productsData, fetchData } = props;
	const [products, setProducts] = useState([]);
	const [id, setId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);
	const [toggle, setToggle] = useState(false);
	const [ordersList, setOrdersList] = useState([]);
	const [rating, setRating] = useState(0);
	const [imageURL, setImageUrl] = useState("");
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	//This calls the modal when the Update Button is clicked
	//It sets showEdit to true triggering the modalwindow for edit to open
	//<Modal show={showEdit} onHide={closeEdit}>
	const openEdit = (productId) => {
		setId(productId);
		fetch(`https://finalcsp.herokuapp.com/products/${productId}`)
			.then((res) => res.json())
			.then((data) => {
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				setRating(data.rating);
				setImageUrl(data.imageURL);
			});
		setShowEdit(true);
	};

	// This closes the Edit Modal Window
	//<Modal show={showEdit} onHide={closeEdit}>
	const closeEdit = () => {
		setName("");
		setDescription("");
		setPrice(0);
		setImageUrl("");
		setRating("");
		setShowEdit(false);
	};

	//This function is called when the Add New Product Button is Clicked
	//This is to add the product from the useStates
	const addProduct = (e) => {
		console.log(name);
		console.log(rating);
		console.log(imageURL);
		e.preventDefault();
		fetch(`https://finalcsp.herokuapp.com/products/`, {
			method: "POST",
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`,
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				rating: rating,
				imageURL: imageURL,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					// this came from login.js. From login.js,  it redirectet to Products and from Products fetchData is passed
					// to AdminView as props
					fetchData();
					alert("Product successfully added.");
					setName("");
					setDescription("");
					setPrice(0);
					closeAdd();
				} else {
					alert("Something went wrong.");
					closeAdd();
				}
			});
	};

	//function to edit product, we are passing the event object and and the productid when the edit button is clicked
	const editProduct = (e, productId) => {
		e.preventDefault();
		fetch(`https://finalcsp.herokuapp.com/products/${productId}`, {
			method: "PUT",
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`,
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				rating: rating,
				imageURL: imageURL,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					fetchData();
					alert("Product successfully updated.");
					setName("");
					setDescription("");
					setPrice(0);
					setRating(0);
					setImageUrl("");
					closeEdit();
				} else {
					alert("Something went wrong.");
					closeEdit();
				}
			});
	};

	useEffect(() => {
		fetch(`https://finalcsp.herokuapp.com/users/orders`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				//the problem here is that the data is present but it is not presented to the frontEnd
				// console.log(data)
				let orders = data.map((user, index) => {
					// console.log(user);
					return (
						<Card key={user.userId}>
							<Accordion>
								<Accordion.Item eventKey={index + 1}>
									<Accordion.Header className="bg-secondary text-white">
										Orders for:
										<span className="text-warning">
											{user.email}
										</span>
									</Accordion.Header>

									<Accordion.Body>
										{user.orders.length > 0 ? (
											user.orders.map((order) => {
												console.log(
													"this is from the mapping"
												);
												console.log(order);
												return (
													<div key={order._id}>
														<h6>
															Purchased on{" "}
															{moment(
																order.purchasedOn
															).format(
																"MM-DD-YYYY"
															)}
															:
														</h6>
														<ul>
															{order.products.map(
																(product) => {
																	return (
																		<li
																			key={
																				product._id
																			}
																		>
																			{
																				product.productName
																			}{" "}
																			-
																			Quantity:{" "}
																			{
																				product.quantity
																			}
																		</li>
																	);
																}
															)}
														</ul>
														<h6>
															Total:{" "}
															<span className="text-warning">
																₱
																{
																	order.totalAmount
																}
															</span>
														</h6>
														<hr />
													</div>
												);
											})
										) : (
											<span>
												No orders for this user yet.
											</span>
										)}
									</Accordion.Body>
								</Accordion.Item>
							</Accordion>
						</Card>
					);
				});
				setOrdersList(orders);
			});
	}, []);

	const toggler = () => {
		if (toggle === true) {
			setToggle(false);
		} else {
			setToggle(true);
		}
	};

	//   This useEffect will work if products are updated from Archive to Active
	useEffect(() => {
		const activateProduct = (productId) => {
			fetch(
				`https://finalcsp.herokuapp.com/products/${productId}/activate`,
				{
					method: "PUT",
					headers: {
						Authorization: `Bearer ${localStorage.getItem(
							"token"
						)}`,
					},
				}
			)
				.then((res) => res.json())
				.then((data) => {
					if (data === true) {
						fetchData();
						alert("Product successfully activated.");
					} else {
						alert("Something went wrong.");
					}
				});
		};

		const archiveProduct = (productId) => {
			alert(productId);

			fetch(
				`https://finalcsp.herokuapp.com/products/${productId}/archive`,
				{
					method: "PUT",
					headers: {
						Authorization: `Bearer ${localStorage.getItem(
							"token"
						)}`,
					},
				}
			)
				.then((res) => res.json())
				.then((data) => {
					if (data === true) {
						fetchData();
						alert("Product successfully archived.");
					} else {
						alert("Something went wrong.");
					}
				});
		};
		//added productData for rating and imageURL
		//To see changes, we must add data to mongodb

		const productsArr = productsData.map((productData) => {
			console.log("testing for product data");
			console.log(productData);
			return (
				<tr key={productData._id}>
					<td>
						<Link to={`/products/${productData._id}`}>
							{productData.name}
						</Link>
					</td>
					<td>{productData.description}</td>
					<td>{productData.price}</td>
					<td>{productData.rating}</td>
					<td>{productData.imageURL}</td>
					<td>
						{productData.isActive ? (
							<span>Available</span>
						) : (
							<span>Unavailable</span>
						)}
					</td>
					<td>
						<Button
							variant="primary"
							size="sm"
							onClick={() => openEdit(productData._id)}
						>
							Update
						</Button>
						{productData.isActive ? (
							<Button
								variant="danger"
								size="sm"
								onClick={() => archiveProduct(productData._id)}
							>
								Disable
							</Button>
						) : (
							<Button
								variant="success"
								size="sm"
								onClick={() => activateProduct(productData._id)}
							>
								Enable
							</Button>
						)}
					</td>
				</tr>
			);
		});

		setProducts(productsArr);
	}, [productsData, fetchData]);

	return (
		<React.Fragment>
			<div className="text-center my-4">
				<h2>Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button
						className="mr-1"
						variant="primary"
						onClick={openAdd}
					>
						Add New Product
					</Button>

					{toggle === false ? (
						<>
							<Button variant="success" onClick={() => toggler()}>
								Show User Orders
							</Button>
							<Link to="/reports">
								<Button className="mr-1" variant="primary">
									Show Reports
								</Button>
							</Link>
						</>
					) : (
						<>
							<Button variant="danger" onClick={() => toggler()}>
								Show Product Details
							</Button>
							<Link to="/reports">
								<Button className="mr-1" variant="primary">
									Show Reports
								</Button>
							</Link>
						</>
					)}
				</div>
			</div>
			{toggle === false ? (
				<Table striped bordered hover responsive>
					<thead className="bg-secondary text-white">
						{/*//added productData for rating and imageURL */}
						{/* //To see changes, we must add data to mongodb */}
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							<th>Rating</th>
							<th>Image URL</th>
							<th>Availability</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>{products}</tbody>
				</Table>
			) : (
				<Accordion>{ordersList}</Accordion>
			)}

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={(e) => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add New Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name:</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter product name"
								value={name}
								onChange={(e) => setName(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description:</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter product description"
								value={description}
								onChange={(e) => setDescription(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price:</Form.Label>
							<Form.Control
								type="number"
								value={price}
								onChange={(e) => setPrice(e.target.value)}
								required
							/>
						</Form.Group>
						{/* Additional feature for imageURL and rating */}
						<Form.Group controlId="productRating">
							<Form.Label>
								Product Rating: (1-5, .5 increment)
							</Form.Label>
							<Form.Control
								type="number"
								value={rating}
								onChange={(e) => setRating(e.target.value)}
								required
							/>
						</Form.Group>
						<Form.Group controlId="imageURL">
							<Form.Label>Image URL:</Form.Label>
							<Form.Control
								type="string"
								value={imageURL}
								onChange={(e) => setImageUrl(e.target.value)}
								required
							/>
						</Form.Group>
						{/* Additional feature for imageURL and rating */}
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>
							Close
						</Button>
						<Button variant="success" type="submit">
							Submit
						</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={(e) => editProduct(e, id)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name:</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter product name"
								value={name}
								onChange={(e) => setName(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description:</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter product description"
								value={description}
								onChange={(e) => setDescription(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price:</Form.Label>
							<Form.Control
								type="number"
								placeholder="Enter product price"
								value={price}
								onChange={(e) => setPrice(e.target.value)}
								required
							/>
						</Form.Group>
						{/* Additional feature for imageURL and rating */}
						<Form.Group controlId="productRating">
							<Form.Label>
								Product Rating: (1-5, .5 increment)
							</Form.Label>
							<Form.Control
								type="number"
								value={rating}
								onChange={(e) => setRating(e.target.value)}
								required
							/>
						</Form.Group>
						<Form.Group controlId="imageURL">
							<Form.Label>Image URL:</Form.Label>
							<Form.Control
								type="string"
								value={imageURL}
								onChange={(e) => setImageUrl(e.target.value)}
								required
							/>
						</Form.Group>
						{/* Additional feature for imageURL and rating */}
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>
							Close
						</Button>
						<Button variant="success" type="submit">
							Submit
						</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</React.Fragment>
	);
};

export default AdminView;

import React from "react";
import { Col, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Rating } from "@mui/material";
import CarousellComp from "../components/CarousellComp";
/*

1. When a product is searched, check what are the props being send.
2. Check where is props coming from. = CustomerView.js


UI:
1. add carousell component
2. Make the boxes smaller and easier to digest

*/
export default function Product(props) {
	const { breakPoint, data } = props;

	const { _id, name, description, price, rating, imageURL } = data;

	return (
		<>
			<Col xs={12} md={3} className="mt-4">
				<Card className="card1">
					<Card.Img variant="top" src={imageURL} />
					<Card.Body>
						<Card.Title className="text-center card2">
							<Link to={`/products/${_id}`}>{name}</Link>
						</Card.Title>
						<Card.Text className="card1">
							<Rating
								name="Rating"
								value={rating}
								precision={0.5}
								readOnly
								align="center"
							/>
						</Card.Text>
						<Card.Text className="card3">{description}</Card.Text>
						<h5 className="text-warning">₱{price}</h5>
					</Card.Body>
					<Card.Footer>
						<Link
							className="btn btn-primary btn-block"
							to={`/products/${_id}`}
						>
							Details
						</Link>
					</Card.Footer>
				</Card>
			</Col>
		</>
	);
}

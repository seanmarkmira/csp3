import React, { useContext, useState } from "react";
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import { Link, Navigate, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import DevicesIcon from "@mui/icons-material/Devices";
import CustomerView from "../components/CustomerView";

/*
	//check if there is data in search click
*/
const AppNavBar = () => {
	const { user, searchProduct, setSearchProduct } = useContext(UserContext);
	const [search, setSearch] = useState("");
	const [products, setProducts] = useState([]);
	const localProduct = [];

	const fuzzySearch = (e) => {
		console.log("TRIGGERED THE FUZZY SEARCH");
		console.log("this is search");
		console.log(search);
		e.preventDefault();
		fetch(`https://finalcsp.herokuapp.com/products/search`, {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({
				name: search,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data);

				data.forEach((data) => {
					console.log("The search has been triggered");
					localProduct.push(data);
					console.log(localProduct);
				});

				setSearchProduct(localProduct);
			});
	};
	return (
		<>
			<Navbar bg="secondary" variant="dark" expand="lg" className="pl-3">
				<Link className="navbar-brand" to="/">
					<DevicesIcon />
					Digi Comp Tech
				</Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="mr-auto">
						<Link className="nav-link" to="/products">
							{user.isAdmin === true ? (
								<span>Admin Dashboard</span>
							) : (
								<span>Products</span>
							)}
						</Link>
					</Nav>
					<Nav className="ml-auto">
						{user.id !== null ? (
							user.isAdmin === true ? (
								<Link className="nav-link" to="/logout">
									Log Out
								</Link>
							) : (
								<React.Fragment>
									<Link className="nav-link" to="/cart">
										Cart
									</Link>
									<Link className="nav-link" to="/orders">
										Orders
									</Link>
									<Link className="nav-link" to="/logout">
										Log Out
									</Link>
								</React.Fragment>
							)
						) : (
							<React.Fragment>
								<Link
									className="nav-link"
									to={{
										pathname: "/login",
										state: { from: "navbar" },
									}}
								>
									Log In
								</Link>
								<Link className="nav-link" to="/register">
									Register
								</Link>
							</React.Fragment>
						)}
					</Nav>
				</Navbar.Collapse>
				<Form className="d-flex" onSubmit={(e) => fuzzySearch(e)}>
					<FormControl
						onChange={(e) => setSearch(e.target.value)}
						type="text"
						placeholder="Use in Product Page"
						className="me-2"
						value={search}
					/>

					<Button
						variant="outline-success"
						className="mr-3"
						type="submit"
					>
						Search
					</Button>
				</Form>
			</Navbar>
		</>
	);
};

export default AppNavBar;

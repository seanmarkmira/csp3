import { useState, useEffect } from "react";
import { CardGroup } from "react-bootstrap";
import Product from "./Product";

export default function Highlights({ data }) {
	const [previews, setPreviews] = useState([]);

	useEffect(() => {
		fetch("https://finalcsp.herokuapp.com/products/active")
			.then((res) => res.json())
			.then((data) => {
				console.log("yey");
				console.log(data);
				const numbers = [];
				const products = [];

				const generateRandomNums = () => {
					let randomNum = Math.floor(Math.random() * data.length);

					if (numbers.indexOf(randomNum) === -1) {
						numbers.push(randomNum);
						console.log("inside if");
					} else {
						generateRandomNums();
						console.log("inside else");
					}
				};
				//There must be an if statement before so that when product is lower than 5 then only use that length rather than just 5. Error on less than 5 products
				for (let i = 0; i < 1; i++) {
					generateRandomNums();

					products.push(
						<Product
							data={data[numbers[i]]}
							key={data[numbers[i]]._id}
							breakPoint={2}
						/>
					);
				}

				setPreviews(products);
			});
	}, []);

	return (
		<CardGroup className="d-flex justify-content-between p-5">
			{previews}
		</CardGroup>
	);
}

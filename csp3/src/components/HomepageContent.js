import { Col, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Rating } from "@mui/material";
import React from "react";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import CarousellComp from "../components/CarousellComp";
import { Carousel } from "react-bootstrap";
import { PhotoCamera } from "@material-ui/icons";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import SportsBarIcon from "@mui/icons-material/SportsBar";
import DevicesIcon from "@mui/icons-material/Devices";
import { useState, useEffect } from "react";
import AccountBalanceRoundedIcon from "@mui/icons-material/AccountBalanceRounded";
import LaptopRoundedIcon from "@mui/icons-material/LaptopRounded";
// List Import
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import PermPhoneMsgRoundedIcon from "@mui/icons-material/PermPhoneMsgRounded";
import AttachMoneyRoundedIcon from "@mui/icons-material/AttachMoneyRounded";
import BeachAccessIcon from "@mui/icons-material/BeachAccess";
import ImageIcon from "@mui/icons-material/Image";
import Divider from "@mui/material/Divider";
// List Import

import { sizing } from "@mui/system";

import {
	Typography,
	Paper,
	CardActions,
	CardContent,
	CardMedia,
	Container,
	Button,
	Grid,
	Chip,
	CardActionArea,
} from "@material-ui/core";

const HomepageContent = () => {
	const [firstSet, setFirstSet] = useState([]);
	const [secondSet, setSecondSet] = useState([]);

	var cardStyle = {
		width: "18vw",
		height: "26vw",
	};

	const fetchActiveProducts = () => {
		fetch("https://finalcsp.herokuapp.com/products/active")
			.then((res) => res.json())
			.then((data) => {
				let firstThree = data.map((product, index) => {
					if (index <= 2) {
						return (
							<Grid item xs={12} sm={3} md={3} key={product._id}>
								{/* <Card sx={{ maxWidth: 120 }}> */}
								<Card style={cardStyle}>
									<CardActionArea>
										{/* <CardMedia */}
										{/* 	component="img" */}
										{/* 	height="140" */}
										{/* 	image={product.imageURL} */}
										{/* 	alt="green iguana" */}
										{/* /> */}
										<Link to={`/products/${product._id}`}>
											<Card.Img
												variant="top"
												src={product.imageURL}
												onClick={() => {}}
											/>
										</Link>
										<CardContent>
											<Typography
												gutterBottom
												variant="h5"
												component="div"
											>
												{product.name}
											</Typography>
											<Typography
												variant="body2"
												className="text-warning"
											>
												₱{product.price}
											</Typography>
										</CardContent>
									</CardActionArea>
									<CardActions>
										{/* <Button size="small" color="primary"> */}
										{/* 	<Link */}
										{/* 		className="btn btn-primary btn-block" */}
										{/* 		to={`/products/${product._id}`} */}
										{/* 	> */}
										{/* 		View Product */}
										{/* 	</Link> */}
										{/* </Button> */}
									</CardActions>
								</Card>
							</Grid>
						);
					}
				});
				setFirstSet(firstThree);

				let secondThree = data.map((product, index) => {
					if (index > 2 && index <= 5) {
						return (
							<Grid item xs={12} sm={3} md={3} key={product._id}>
								{/* <Card sx={{ maxWidth: 120 }}> */}
								<Card style={cardStyle}>
									<CardActionArea>
										{/* <CardMedia */}
										{/* 	component="img" */}
										{/* 	height="140" */}
										{/* 	image={product.imageURL} */}
										{/* 	alt="green iguana" */}
										{/* /> */}
										<Link to={`/products/${product._id}`}>
											<Card.Img
												variant="top"
												src={product.imageURL}
												onClick={() => {}}
											/>
										</Link>
										<CardContent>
											<Typography
												gutterBottom
												variant="h5"
												component="div"
											>
												{product.name}
											</Typography>
											<Typography
												variant="body2"
												className="text-warning"
											>
												₱{product.price}
											</Typography>
										</CardContent>
									</CardActionArea>
									<CardActions>
										{/* <Button size="small" color="primary"> */}
										{/* 	<Link */}
										{/* 		className="btn btn-primary btn-block" */}
										{/* 		to={`/products/${product._id}`} */}
										{/* 	> */}
										{/* 		View Product */}
										{/* 	</Link> */}
										{/* </Button> */}
									</CardActions>
								</Card>
							</Grid>
						);
					}
				});
				setSecondSet(secondThree);
			});
	};

	useEffect(() => {
		fetchActiveProducts();
	}, []);

	return (
		<Grid container spacing={3}>
			<Grid item xs={12} sm={3} md={3}>
				{/* FIRST LIST */}
				<List
					sx={{
						width: "100%",
						maxWidth: 360,
						bgcolor: "background.paper",
					}}
				>
					<ListItem>
						<ListItemAvatar>
							<Avatar>
								<PermPhoneMsgRoundedIcon />
							</Avatar>
						</ListItemAvatar>
						<ListItemText
							primary="Call for stocks"
							secondary="Some items are order basis from supplier. Always call for stock availability."
						/>
					</ListItem>
					<Divider variant="inset" component="li" />
					<ListItem>
						<ListItemAvatar>
							<Avatar>
								<AttachMoneyRoundedIcon />
							</Avatar>
						</ListItemAvatar>
						<ListItemText
							primary="Cash Basis"
							secondary="Full payment is required for reservations/orders."
						/>
					</ListItem>
					<Divider variant="inset" component="li" />
					<ListItem>
						<ListItemAvatar>
							<Avatar>
								<ImageIcon />
							</Avatar>
						</ListItemAvatar>
						<ListItemText
							primary="Images on Site"
							secondary="Images posted are for reference only."
						/>
					</ListItem>
					<Divider variant="inset" component="li" />
					<ListItem>
						<ListItemAvatar>
							<Avatar>
								<AccountBalanceRoundedIcon />
							</Avatar>
						</ListItemAvatar>
						<ListItemText
							primary="Checks/Banks"
							secondary="Cheque payments are subject to 3 banking days clearing."
						/>
					</ListItem>
					<Divider variant="inset" component="li" />
					<ListItem>
						<ListItemAvatar>
							<Avatar>
								<LaptopRoundedIcon />
							</Avatar>
						</ListItemAvatar>
						<ListItemText
							primary="Change of Prices/Specs"
							secondary="Prices and specifications may change without prior notice."
						/>
					</ListItem>
				</List>
				{/* First List */}
			</Grid>

			{firstSet}
			<Grid item xs={12} sm={3} md={3}>
				<Paper elevation={1} sx={{ p: 3 }}>
					<div>
						<Divider>
							<Chip label="Mark Z." />
						</Divider>
						This Ecommerce, does exactly what it's suppose to do.
						<Divider>
							<Chip label="Jeff B." />
						</Divider>
						Friendlier customer service compared to Amazon.
						<Divider>
							<Chip label="Elon M." />
						</Divider>
						Need more variety products, like electric cars or
						motors?
						<Divider>
							<Chip label="Vlad P." />
						</Divider>
						Best place to order bulk computers for campaigns.
					</div>
				</Paper>
			</Grid>
			{secondSet}
		</Grid>
	);
};

export default HomepageContent;

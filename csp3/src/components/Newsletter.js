import React from "react";
import { Carousel, Container, Row, Col } from "react-bootstrap";
import {
	Paper,
	Typography,
	ListItem,
	Grid,
	Box,
	Button,
	TextField,
} from "@mui/material";
import Stack from "@mui/material/Stack";

const Newsletter = () => {
	return (
		<>
			<div className="text-center">
				<Grid container justifyContent="center" alignItems="center">
					<Row>
						<Typography variant="h4">Newsletter</Typography>

						<Col>
							<Typography variant="subtitle2">
								Enter your e-mail address to get the latest
								deals and last minute sale!
							</Typography>
							<Stack
								direction="row"
								spacing={2}
								className="text-center"
							>
								<TextField
									id="outlined-basic"
									label="Email Address"
									variant="outlined"
								/>
								<Button variant="contained">Submit</Button>
							</Stack>
						</Col>
					</Row>
				</Grid>
			</div>
		</>
	);
};
export default Newsletter;

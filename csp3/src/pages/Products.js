import React, { useState, useEffect, useContext } from "react";
import AdminView from "../components/AdminView";
import CustomerView from "../components/CustomerView";
import { Container } from "react-bootstrap";

import UserContext from "../UserContext";

//if there is data on the product data, use that to push to products
export default function Products() {
	const { user, searchProduct } = useContext(UserContext);

	const [products, setProducts] = useState([]);

	const fetchData = () => {
		if (searchProduct.length == 0) {
			console.log("blank");
			fetch(`https://finalcsp.herokuapp.com/products/active`)
				.then((res) => res.json())
				.then((data) => {
					setProducts(data);
				});
		} else {
			console.log("with data");
			setProducts(searchProduct);
		}
	};

	useEffect(() => {
		fetchData();
	}, [searchProduct]);

	return (
		<Container>
			{user.isAdmin === true ? (
				<AdminView productsData={products} fetchData={fetchData} />
			) : (
				<CustomerView productsData={products} />
			)}
		</Container>
	);
}

import React, { useState, useEffect } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Row, Col } from "react-bootstrap";

const Reports = () => {
	const [productSales, setProductSales] = useState([]);
	const [customerSales, setCustomerSales] = useState([]);
	const [vipCustomer, setVIPCustomer] = useState([]);

	useEffect(() => {
		fetch(`https://finalcsp.herokuapp.com/products/report/saleable`)
			.then((res) => res.json())
			.then((data) => {
				let productSalesResult = data.map((item, index) => {
					return (
						<TableBody key={item.productId}>
							<TableRow>
								<TableCell component="th" scope="row">
									{item.productName}
								</TableCell>
								<TableCell align="right">
									{item.productId}
								</TableCell>
								<TableCell align="right">
									{item["Total Checkouts with Item"]}
								</TableCell>
								<TableCell align="right">
									{item["Total Quantity Sold"]}
								</TableCell>
							</TableRow>
						</TableBody>
					);
				});
				setProductSales(productSalesResult);
			});

		fetch(`https://finalcsp.herokuapp.com/products/report/sales`)
			.then((res) => res.json())
			.then((data) => {
				let customerSalesResult = data.map((item, index) => {
					return (
						<TableBody key={item.Username}>
							<TableRow>
								<TableCell component="th" scope="row">
									{item.Username}
								</TableCell>
								<TableCell align="right">
									₱{item["Total Sales"]}
								</TableCell>
								<TableCell align="right">
									₱{item["Average Sales per Checkout"]}
								</TableCell>
								<TableCell align="right">
									₱{item["Highest Checkout"]}
								</TableCell>
								<TableCell align="right">
									₱{item["Lowest Checkout"]}
								</TableCell>
							</TableRow>
						</TableBody>
					);
				});

				setCustomerSales(customerSalesResult);
			});

		fetch(`https://finalcsp.herokuapp.com/products/report/vip`)
			.then((res) => res.json())
			.then((data) => {
				let vipCustomerResult = data.map((item, index) => {
					return (
						<TableBody key={item.username}>
							<TableRow>
								<TableCell component="th" scope="row">
									{item.username}
								</TableCell>
								<TableCell align="right">
									{item["total checkouts"]}
								</TableCell>
							</TableRow>
						</TableBody>
					);
				});

				setVIPCustomer(vipCustomerResult);
			});
	}, []);

	return (
		<>
			<Row>
				<Col>
					<div className="text-center">
						<h1>Reports Dashboard</h1>
					</div>
				</Col>
			</Row>

			<Row>
				<Col>
					<div className="text-center mt-4">
						<h4>Saleable Products</h4>
						<p>Minor bug found due to change of model.</p>
					</div>
				</Col>
			</Row>

			<TableContainer component={Paper}>
				<Table sx={{ minWidth: 650 }} aria-label="simple table">
					<TableHead>
						<TableRow>
							<TableCell>Product Name</TableCell>
							<TableCell align="right">Product ID</TableCell>
							<TableCell align="right">
								Total Checkouts with Item
							</TableCell>
							<TableCell align="right">
								Total Quantity Sold
							</TableCell>
						</TableRow>
					</TableHead>
					{productSales}
				</Table>
			</TableContainer>
			<Row>
				<Col>
					<div className="text-center mt-4">
						<h4>Sales per Customer</h4>
					</div>
				</Col>
			</Row>
			<TableContainer component={Paper}>
				<Table sx={{ minWidth: 650 }} aria-label="simple table">
					<TableHead>
						<TableRow>
							<TableCell>Username</TableCell>
							<TableCell align="right">Total Sales</TableCell>
							<TableCell align="right">
								Average Sales per Checkout
							</TableCell>
							<TableCell align="right">
								Highest Checkout
							</TableCell>
							<TableCell align="right">Lowest Checkout</TableCell>
						</TableRow>
					</TableHead>
					{customerSales}
				</Table>
			</TableContainer>

			<Row>
				<Col>
					<div className="text-center mt-4">
						<h4>VIP Customer</h4>
					</div>
				</Col>
			</Row>
			<TableContainer component={Paper}>
				<Table sx={{ minWidth: 650 }} aria-label="simple table">
					<TableHead>
						<TableRow>
							<TableCell>Username</TableCell>
							<TableCell align="right">Total Checkouts</TableCell>
						</TableRow>
					</TableHead>
					{vipCustomer}
				</Table>
			</TableContainer>
		</>
	);
};

export default Reports;

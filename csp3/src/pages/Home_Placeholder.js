import React from "react";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import { Container } from "react-bootstrap";

const Home = () => {
	const pageData = {
		title: "The Zuitt Shop",
		content: "Products for everyone, everywhere",
		destination: "/products",
		label: "Browse Products",
	};
	console.log("loaded home");
	console.log(process.env.REACT_APP_API_URL);
	return (
		<>
			<Banner data={pageData} />
			<Container fluid>
				<h2 className="text-center mb-4">Featured Products</h2>
				<Highlights />
			</Container>
		</>
	);
};
export default Home;

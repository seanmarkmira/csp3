import React, { useState, useEffect, useContext } from "react";
import {
	Container,
	Col,
	Card,
	Button,
	Form,
	InputGroup,
	FormControl,
	Row,
	Stack,
} from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import { Rating } from "@mui/material";
import UserContext from "../UserContext";
import { Typography } from "@mui/material";
//to Accomplish:
// if press + add 1 to text field (done)
// if press - minus 1 to text field (done)
// if there are changes within the text value, update the price (still working on this)
// Note: For the prevState tutorial please see the 45. Functional Update form to study how to update current values of the page
//Next is to add to cart:
// Once add to cart, we will fetch the information presented and will pass to the mongoDB

const Specific = () => {
	const { user } = useContext(UserContext);
	const { productId } = useParams();

	const [id, setId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [qty, setQty] = useState(1);
	const [price, setPrice] = useState(0);
	const [rating, setRating] = useState(0);
	const [imageURL, setImageURL] = useState("");

	useEffect(() => {
		fetch(`https://finalcsp.herokuapp.com/products/${productId}`)
			.then((res) => res.json())
			.then((data) => {
				setId(data._id);
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				setRating(data.rating);
				setImageURL(data.imageURL);
			});
	});

	const reduceQty = () => {
		if (qty <= 1) {
			alert("Quantity can't be lower than 1.");
		} else {
			setQty(qty - 1);
		}
	};

	const addToCart = () => {
		let alreadyInCart = false;
		let productIndex;
		let message;
		let cart = [];

		if (localStorage.getItem("cart")) {
			cart = JSON.parse(localStorage.getItem("cart"));
		}

		for (let i = 0; i < cart.length; i++) {
			if (cart[i].productId === id) {
				alreadyInCart = true;
				productIndex = i;
			}
		}

		if (alreadyInCart) {
			cart[productIndex].quantity += qty;
			cart[productIndex].subtotal =
				cart[productIndex].price * cart[productIndex].quantity;
		} else {
			cart.push({
				productId: id,
				name: name,
				price: price,
				quantity: qty,
				subtotal: price * qty,
			});
		}

		localStorage.setItem("cart", JSON.stringify(cart));

		if (qty === 1) {
			message = "1 item added to cart.";
		} else {
			message = `${qty} items added to cart.`;
		}

		alert(message);
	};

	const qtyInput = (value) => {
		if (value === "") {
			value = 1;
		} else if (value === "0") {
			alert("Quantity can't be lower than 1.");
			value = 1;
		}

		setQty(value);
	};

	return (
		<Container>
			<Row>
				<Col md={5} xs={12}>
					<Card className="mt-4">
						<Card.Header className="bg-secondary text-white text-center pb-0">
							<h4>Product Details</h4>
						</Card.Header>
						<Card.Img
							variant="top"
							src={imageURL}
							className="specificPhoto"
						/>
						{/* <Card.Body> */}
						{/* 	<Card.Text>{description}</Card.Text> */}
						{/* 	<h6> */}
						{/* 		Price:{" "} */}
						{/* 		<span className="text-warning">₱{price}</span> */}
						{/* 	</h6> */}
						{/* 	<h6>Quantity:</h6> */}
						{/* 	<InputGroup className="qty mt-2 mb-1"> */}
						{/* 		<InputGroup className="d-none d-md-flex"> */}
						{/* 			<Button */}
						{/* 				variant="secondary" */}
						{/* 				onClick={reduceQty} */}
						{/* 			> */}
						{/* 				- */}
						{/* 			</Button> */}
						{/* 		</InputGroup> */}
						{/* 		<FormControl */}
						{/* 			type="number" */}
						{/* 			min="1" */}
						{/* 			value={qty} */}
						{/* 			onChange={(e) => qtyInput(e.target.value)} */}
						{/* 		/> */}
						{/* 		<InputGroup className="d-none d-md-flex"> */}
						{/* 			<Button */}
						{/* 				variant="secondary" */}
						{/* 				onClick={() => setQty(qty + 1)} */}
						{/* 			> */}
						{/* 				+ */}
						{/* 			</Button> */}
						{/* 		</InputGroup> */}
						{/* 	</InputGroup> */}
						{/* </Card.Body> */}
						{/* <Card.Footer> */}
						{/* 	{user.id !== null ? ( */}
						{/* 		user.isAdmin === true ? ( */}
						{/* 			<Button variant="danger" block disabled> */}
						{/* 				Admin can't Add to Cart */}
						{/* 			</Button> */}
						{/* 		) : ( */}
						{/* 			<Button */}
						{/* 				variant="primary" */}
						{/* 				block */}
						{/* 				onClick={addToCart} */}
						{/* 			> */}
						{/* 				Add to Cart */}
						{/* 			</Button> */}
						{/* 		) */}
						{/* 	) : ( */}
						{/* 		<Link */}
						{/* 			className="btn btn-warning btn-block" */}
						{/* 			to={{ */}
						{/* 				pathname: "/login", */}
						{/* 				state: { from: "cart" }, */}
						{/* 			}} */}
						{/* 		> */}
						{/* 			Log in to Add to Cart */}
						{/* 		</Link> */}
						{/* 	)} */}
						{/* </Card.Footer> */}
					</Card>
				</Col>

				<Col md={7} xs={12} className="mt-4">
					<Typography variant="h5" gutterBottom>
						{name}
					</Typography>
					<Rating
						name="Rating"
						value={rating}
						precision={0.5}
						readOnly
						align="center"
					/>
					<Typography variant="subtitle1">QUICK OVERVIEW:</Typography>
					<Typography variant="subtitle1">{description}</Typography>
					<hr />
					<Typography variant="h4" className="text-warning">
						₱{price}
					</Typography>

					<Stack direction="horizontal" gap={5}>
						<h6>Quantity:</h6>
						{/* <InputGroup className="qty mt-2 mb-1"> */}
						{/* 	<InputGroup className="d-none d-md-flex"> */}
						<Stack direction="horizontal" gap={0}>
							<Button variant="secondary" onClick={reduceQty}>
								-
							</Button>
							{/* </InputGroup> */}
							<FormControl
								type="number"
								min="1"
								value={qty}
								onChange={(e) => qtyInput(e.target.value)}
								className="inputQuantity"
							/>
							{/* <InputGroup className="d-none d-md-flex"> */}
							<Button
								variant="secondary"
								onClick={() => setQty(qty + 1)}
							>
								+
							</Button>
							{/* 	</InputGroup> */}
							{/* </InputGroup> */}
						</Stack>
					</Stack>
					{user.id !== null ? (
						user.isAdmin === true ? (
							<Button
								variant="danger"
								block
								disabled
								className="mt-3"
							>
								Admin can't Add to Cart
							</Button>
						) : (
							<Button
								variant="primary"
								block
								onClick={addToCart}
								className="mt-3"
							>
								Add to Cart
							</Button>
						)
					) : (
						<Link
							className="btn btn-warning btn-block mt-4"
							to={{
								pathname: "/login",
								state: { from: "cart" },
							}}
						>
							Log in to Add to Cart
						</Link>
					)}
				</Col>
			</Row>

			{/* <Col md={12} xs={12}> */}
			{/* 	<Row>Related Products</Row> */}
			{/* </Col> */}
		</Container>
	);
};

export default Specific;

//This is personal code
// const Specific = () => {
// 	const [rating, setRating] = useState(0);
// 	const { productId } = useParams();
// 	const [products, setProducts] = useState([]);
// 	let [quantity, setQuantity] = useState(0);
// 	let [cost, setCost] = useState();
//
// 	const fetchData = () => {
// 		fetch(`http://localhost:4000/products/${productId}`)
// 			.then((res) => res.json())
// 			.then((data) => {
// 				setProducts(data);
// 				setRating(data.rating);
// 				setCost(data.price);
// 			});
// 	};
//
// 	useEffect(() => {
// 		fetchData();
// 		setQuantity(1);
// 	}, []);
//
// 	const updatePrice = (variable) => {
// 		console.log("trigger updateprice");
// 		if (variable == 1) {
// 			setQuantity((prevState) => {
// 				return prevState + 1;
// 			});
// 			//the problem is that we need to get the setQuantity result
// 			setCost((prevState) => {
// 				return products.price * quantity;
// 			});
// 		} else if (variable == -1) {
// 			setQuantity((prevState) => {
// 				return prevState - 1;
// 			});
// 			setCost((prevState) => {
// 				return products.price * quantity;
// 			});
// 		}
// 	};
//
// 	return (
// 		<>
// 			<Col xs={5} className="my-5 mx-auto">
// 				<Card
// 					className="card1"
// 					border="dark"
// 					// style={{ width: "25rem" }}
// 				>
// 					<Card.Header className="text-center card2">
// 						{products.name}
// 					</Card.Header>
//
// 					<Card.Img
// 						variant="top"
// 						src={products.imageURL}
// 						className="specificPhoto"
// 					/>
// 					<Rating
// 						name="Rating"
// 						value={rating}
// 						precision={0.5}
// 						readOnly
// 					/>
// 					<Card.Body>
// 						<Card.Text>{products.description}</Card.Text>
// 						<h6 className="text-warning">₱{cost}</h6>
// 						<Form.Label>Quantity:</Form.Label>
// 						<Button
// 							variant="dark"
// 							id="addition"
// 							onClick={() => {
// 								updatePrice(1);
// 							}}
// 						>
// 							+
// 						</Button>{" "}
// 						<Button
// 							variant="dark"
// 							id="subtraction"
// 							onClick={() => {
// 								updatePrice(-1);
// 							}}
// 						>
// 							-
// 						</Button>{" "}
// 						<Form.Control type="number" value={quantity} />
// 						<Form.Text>
// 							Enter the desired number of quantity.
// 						</Form.Text>
// 						<br />
// 					</Card.Body>
// 					<Card.Footer className="text-muted, text-center">
// 						<Button variant="primary" size="lg" active>
// 							Add to Cart
// 						</Button>
// 					</Card.Footer>
// 				</Card>
// 			</Col>
// 		</>
// 	);
// };
//
// export default Specific;
//
//

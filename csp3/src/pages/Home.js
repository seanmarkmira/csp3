import React from "react";
import CarousellComp from "../components/CarousellComp";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import { useState, useEffect } from "react";
import HomepageContent from "../components/HomepageContent";
import { Typography, Container, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";

const Home = () => {
	useEffect(() => {
		fetch("https://finalcsp.herokuapp.com/products/active")
			.then((res) => res.json())
			.then((data) => {
				console.log("These are the active products");
				console.log(data);
			});
	}, []);

	return (
		<>
			<main>
				<div align="center" className="mt-5">
					<Container maxWidth="sm">
						<Typography
							variant="h2"
							align="center"
							color="textPrimary"
							gutterBottom
						>
							Digi Comp Tech
						</Typography>

						<Button
							variant="contained"
							endIcon={<AddShoppingCartIcon />}
							size="large"
							// onClick={()=>{ to={`/products/`}}
						>
							<Link to={`/products/`} className="customLink">
								Browse All Products
							</Link>
						</Button>

						<Typography
							variant="h5"
							align="center"
							color="textSecondary"
							paragraph
						>
							Tech Products for Everyone, Everywhere
						</Typography>
					</Container>
				</div>
				<CarousellComp />
				<HomepageContent />
			</main>
		</>
	);
};
export default Home;
// const Home = () => {
// 	const pageData = {
// 		title: "The Zuitt Shop",
// 		content: "Products for everyone, everywhere",
// 		destination: "/products",
// 		label: "Browse Products",
// 	};
// 	console.log("loaded home");
// 	console.log(process.env.REACT_APP_API_URL);
// 	return (
// 		<>
// 			<Banner data={pageData} />
// 			<Container fluid>
// 				<h2 className="text-center mb-4">Featured Products</h2>
// 				<Highlights />
// 			</Container>
// 		</>
// 	);
// };
// export default Home;
